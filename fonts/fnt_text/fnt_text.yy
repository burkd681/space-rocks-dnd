{
    "id": "9c86d7b2-2f29-4b2a-b22c-8ec519a51ba5",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_text",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Arial CE",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "1dc96316-70ce-48f3-b7e1-7f28088652ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 21,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "7ae606a2-60e5-4d10-90ef-7739ee77546b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 21,
                "offset": 2,
                "shift": 6,
                "w": 3,
                "x": 235,
                "y": 48
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "998882ff-6707-4de7-847d-ad4be03d769d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 226,
                "y": 48
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "6f46922d-dc01-472f-b249-796247852114",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 214,
                "y": 48
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "cbab6207-a255-4442-96ac-449bcd88cb20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 203,
                "y": 48
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "0f46b4d6-4019-4be5-b50b-dbbf7e6a6dc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 21,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 186,
                "y": 48
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "79c114ad-9a4a-49a3-859e-852c0f8f7908",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 172,
                "y": 48
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "96032edd-1607-4fc8-9e46-61828cb97ca9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 167,
                "y": 48
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "e32c71d9-a1d7-43d1-b8ee-5fbbf2d87ef7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 160,
                "y": 48
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "1e9894fc-c82f-47b0-8d79-290339f9123c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 154,
                "y": 48
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "9592e97b-6e44-4a0b-8b0e-c08524d25d5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 21,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 240,
                "y": 48
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "86de1ecb-bc18-43ea-8413-d8139dcc4693",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 143,
                "y": 48
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "9f700443-38a2-4dbf-9312-cc0f7fe1ca8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 127,
                "y": 48
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "ade2ae70-f230-4427-b678-5f0cf5af92de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 120,
                "y": 48
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "bddce52e-81e5-4529-a234-41ebd52c25df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 115,
                "y": 48
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "beb874e4-525a-46c8-ac97-0e18e624512f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 21,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 108,
                "y": 48
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "865f5d62-bf0c-4797-bea9-c75cd9eabee8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 97,
                "y": 48
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "45d461ee-e4ea-4f7c-81db-c25fd5fdf38d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 21,
                "offset": 2,
                "shift": 11,
                "w": 5,
                "x": 90,
                "y": 48
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "cd8270ee-3fe8-4bf5-a6ff-edae2fd3a9bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 78,
                "y": 48
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "fd3a6748-fbea-4357-afc9-e229b5c21a52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 67,
                "y": 48
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "dadb1513-1a61-4500-a204-2a7a35f5c8bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 55,
                "y": 48
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "c6d67784-e7d4-4c21-8b89-b0ad61985a50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 132,
                "y": 48
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "f069ccf5-9a20-4d2e-946e-3a92e9ec3a08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 2,
                "y": 71
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "9b149bbb-246f-4dd5-b61a-e1d10030a1c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 13,
                "y": 71
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "6122186c-3e45-47b6-b6b8-85eb13fbab7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 24,
                "y": 71
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "921de3af-e3e9-4487-8159-a001c83322f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 32,
                "y": 94
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "117b8073-15f7-4d43-84ba-500dfbb62a94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 21,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 28,
                "y": 94
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "6552a159-3746-40dd-a5d8-958c7687a805",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 21,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 24,
                "y": 94
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "29dcb540-0796-49a1-9431-048487fb56df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 13,
                "y": 94
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "d63ca4aa-c401-44e0-b19d-955ad703bd86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 2,
                "y": 94
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "38192fd5-1316-4726-a52e-95fe9bce4856",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 235,
                "y": 71
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "790e4ee0-dc92-4c17-8202-39ea3aa7b0fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 223,
                "y": 71
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "f1f761c0-4a5a-411a-a781-bb00592f55bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 21,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 204,
                "y": 71
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "486294b4-c7b3-4bea-94fc-03929d65ac0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 21,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 188,
                "y": 71
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "b09b4411-2ace-419b-960f-a2014ca5dbef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 174,
                "y": 71
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "5d445bac-ab7a-4d4a-8da3-8ea007860603",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 160,
                "y": 71
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "32ab08c0-a2c1-4173-b6cc-733ca75557a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 146,
                "y": 71
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "372a3030-2e7e-4f03-a852-3eb6ac8f5dfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 133,
                "y": 71
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "7fd7a89f-d5e1-4f1e-a513-27b304cd4730",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 121,
                "y": 71
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "9039fa43-ff0f-40e9-bbab-60b2f5d99544",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 21,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 106,
                "y": 71
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "b85a8a4e-8215-45ff-86aa-6f232cbb2ec3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 93,
                "y": 71
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "0ba9162d-7087-4372-b82f-9f3cabf9a639",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 88,
                "y": 71
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "de3f6729-6c48-4740-bcec-d0f22ae0b47a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 77,
                "y": 71
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "024dbe31-a5f2-4566-81fb-0fdcc9f1e971",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 62,
                "y": 71
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "2b81aab5-e5d2-4fec-8461-938a80ed6e74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 50,
                "y": 71
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "0f854b65-8af4-4c63-b43a-3739758f705f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 21,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 35,
                "y": 71
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "d1cbc52a-e233-44d0-91be-2317685e6541",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 42,
                "y": 48
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "cb0cb09e-12de-4d94-bc06-4d438cc768a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 21,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 27,
                "y": 48
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "3de2ecdb-dae0-4f28-bcb1-5a87f874aa7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 14,
                "y": 48
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "8e63ce43-56f0-4dd2-9b4d-a92896170dea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 21,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 9,
                "y": 25
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "cffcb7b9-5d83-42f6-ab32-b1882844637b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 236,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "67168250-f48e-41d9-98d0-744c686c42d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 223,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "a98ef94b-add1-43ff-8b82-63e80f849a86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 21,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 210,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "b5040925-604f-45c8-a390-624cacb61530",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 197,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "4bc36b16-d089-4391-9739-9722c5ed18c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 21,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 182,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "02410fe6-48b3-47bd-8ba5-4c182e39993d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 21,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 162,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "3b8a8e34-6d19-4b9a-b2b5-36bff8c030ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 21,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 147,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "0c515cf2-fe43-4518-aeba-3e0c05f9b246",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 21,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 132,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "35ef120f-f6b0-4f1e-90be-3aaa6c842483",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 21,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 119,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "1ea71706-4019-4f10-897b-1e0c0677ab1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 2,
                "y": 25
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "9c38eb36-0ee0-4122-924d-296fd5a63a71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 21,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "3f89efa0-a7a4-41b1-81e6-0e1ad5c0c315",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 96,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "eedd3fb1-23a3-44d2-963d-229b5d8b440e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 85,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "78f827f0-fb15-46ce-ab13-76ee0804aec6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "0ea1cdf5-92cc-4fae-b140-546f57edbeeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 65,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "a193d8b3-8d23-4554-8cd2-4a27b15e6a5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "3d2f5522-abda-4580-b52b-8a1f3933b1c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 42,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "13add4df-ffb3-42cc-b7b8-00aa52091285",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 31,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "1b7c139d-079e-4742-a185-a5fa96058ab2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 20,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "7392f919-fad7-4c62-a843-6d23775d575a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 9,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "0624ef6e-76a7-4359-822c-204dbd3a9206",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 103,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "4d8fa6d1-8131-42c6-a578-c09b969481e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 25,
                "y": 25
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "56801a16-33b3-4e01-8f08-0028d27c0f9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 137,
                "y": 25
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "ff442347-9694-4d57-b038-029d80b75a7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 36,
                "y": 25
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "9b4be75a-8515-47b3-bd4d-1461fc5883a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 21,
                "offset": -1,
                "shift": 5,
                "w": 5,
                "x": 244,
                "y": 25
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "29448d37-f814-43d6-a2d9-199bb260c71e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 233,
                "y": 25
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "c7ba56b0-6a3e-4138-af6b-e211400e905c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 228,
                "y": 25
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "05cf1c1e-c4b4-4329-bcbe-39cc5d235f30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 21,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 211,
                "y": 25
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "1350a588-5378-4bba-b4bb-97a65bbde7ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 200,
                "y": 25
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "aa1d9976-6d18-40ec-936e-0ad92725e697",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 188,
                "y": 25
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "1a9be797-477c-4c2a-a887-59ea7b1bc9c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 176,
                "y": 25
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "56be9221-f3b8-4091-948e-b25805447eb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 165,
                "y": 25
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "6daa09ad-7a09-43de-9ed4-e6be9371d27d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 156,
                "y": 25
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "b80a0b83-913f-4803-ba7a-610d6d73458b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 2,
                "y": 48
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "7fde345d-086c-46af-8946-8288ba9b8b10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 148,
                "y": 25
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "f0a9e189-496c-401a-b00c-d457af5e9ac0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 126,
                "y": 25
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "496722e7-6bdb-4f9a-a1a1-7b55fdb48745",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 114,
                "y": 25
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "c9f3f8ce-1629-4bbd-862f-a1ea55fc42de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 97,
                "y": 25
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "b2ae7638-b26a-4990-890a-159d16b02ba1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 85,
                "y": 25
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "68f88658-c69e-4c97-a4ec-b46aa25bfdd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 73,
                "y": 25
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "89e1a8dd-a7b5-4af0-90d0-9b677e0d2e73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 62,
                "y": 25
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "03db6468-9d8e-4cda-81f5-815820f5a031",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 54,
                "y": 25
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "a9111db6-0e65-42f5-b99c-29ee5d592aa3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 21,
                "offset": 2,
                "shift": 5,
                "w": 2,
                "x": 50,
                "y": 25
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "73d7b7a9-e340-4b85-850b-b0db4f9a91f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 21,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 41,
                "y": 25
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "62df8e16-dfee-4d2f-b292-d9980a8fdc6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 43,
                "y": 94
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "16d551f5-6c1f-4f45-9b5c-afaa62a5f81c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 21,
                "offset": 4,
                "shift": 18,
                "w": 11,
                "x": 54,
                "y": 94
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "8ee40784-95ef-4cab-bb3e-985b0d9b94f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "c3de5995-3002-45bb-8467-f0209711fb0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 902
        },
        {
            "id": "e38cb0ce-666a-4c98-be52-afe4c4edc252",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 913
        },
        {
            "id": "c899e2c8-0152-47cd-8588-a322878bb4a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 916
        },
        {
            "id": "df1dd9c9-7f19-412a-93a5-fb48e7527aeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 923
        },
        {
            "id": "2973d5a4-6e38-4c93-9a47-5a0f06c402f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "199f40d6-ea75-4152-a3e6-d222cdb38e8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "46d9727b-fe59-40f6-ac63-19e692f5f704",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "c34764a4-6a1e-40d5-9691-c6ba61f90e52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "1ec52d98-1c11-42aa-8c5a-58826236904c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "8e4a4a18-10df-4371-bed7-01d2cfd40f83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "114eb0b1-875d-4bfe-b33d-30e338a2a418",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "8c057aea-d15b-4eec-94b9-83b474437016",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 121
        },
        {
            "id": "5b1be23d-a580-4ce4-b455-2b0300b5f165",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 160
        },
        {
            "id": "2a63be62-007a-4f00-865c-9deba76406e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "9b81fb6c-f1af-4881-9d8c-5e3c00205ad7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 44
        },
        {
            "id": "e3a4e495-be4e-4323-912a-f3e945223cda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 46
        },
        {
            "id": "a36aadb3-56d8-4247-8e79-1b1c74f27995",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "ead2d3f4-2e42-4304-af19-4c5febec64a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "800c04cd-4292-4daf-b9c0-8bcd739977b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "a660fe24-692a-4cd6-ad98-ebb9464750ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "9fa8856d-3fc8-40a8-8b76-bfc7f363ed67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "4ea5acfc-2333-4e9b-a67b-3ad9fff0c6fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "00b2420a-9a83-4c4f-9a59-4126186bbd74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "5ca16a29-906b-405c-8a20-7949053895e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 44
        },
        {
            "id": "dd4c4e14-7daa-4d15-a5ca-bbb8d0921f35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 46
        },
        {
            "id": "5b05f486-c6e1-497d-befa-b58b7492075c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "f81b687e-dfd7-4518-bd93-32256f483356",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 89
        },
        {
            "id": "873b20bc-ee95-4252-9f66-8d391ca97dbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 44
        },
        {
            "id": "99b5080f-93a4-47d9-ad9b-8e8c840a8c0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "e752aaf9-5f25-4119-b360-3b8cc5ce9115",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 46
        },
        {
            "id": "d8135b33-05ba-49fa-b7c3-28ff467751ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 58
        },
        {
            "id": "5a5a787a-1e30-4e85-b335-088f9da9c82e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 59
        },
        {
            "id": "83af93bf-f26f-4446-86a0-6466ee929930",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "5868e4af-2b58-4fb7-b2c7-c9b88bd78417",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "e1851684-3153-46c2-8991-b239b4e7fb4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "93cbb004-a078-43d7-ad9e-f6af9bf9e4e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "c9b0ff66-c0f2-4660-95ae-60bbe6b5488b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "400e1110-5950-4e3f-bdfb-80f6df5267b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "5fad0ab6-299d-4981-b165-0c1c14b09cf5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "6590441d-ace2-4cad-83c8-991fd7c04f2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "f9d50659-4c59-4171-8212-ff1d6bd9767b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "23d3e9a0-f960-4976-8377-8c679c287b76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "08035e11-5210-41b1-a940-0bb9852bc08b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "45a58f2a-11a9-4073-8e7e-cb662e271b28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 894
        },
        {
            "id": "68f15936-63b8-4bf5-8269-95621adf350c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "478c235e-3305-4940-bf9b-df1ad9bc654e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "ee689611-75f2-43e2-b919-e204fff1e5c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "bcb0e10d-f951-41e3-9a90-9eb88f8a58c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "13df6f85-38fa-48a3-ab8d-7ce05a0ff05f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "a1144e9f-3411-473f-b313-0c4cdd2edef4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "4a168508-27c2-45ea-806d-8a3c5e3c13a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "8eb7c25b-31be-4c7f-b478-fa47af2de1bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "b06de50a-8196-4676-8f4d-6a9b270a421b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "ea476e35-cd74-4b18-94bc-15cdd833b8c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "099114b8-5881-4d43-920f-5e1df8db6f3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "ac70dc30-1b73-40d6-b7e5-a4a7ac48e31e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "5991c6af-00d1-44cf-a3aa-9f0635a71163",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "dc7ecfb4-3448-4018-8419-1b152064b760",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "860b26aa-5fd5-46b9-b74b-6e44914a4d16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "ec2e2664-bbfe-4ceb-9630-7be880a92a81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "bf4e040e-c6d9-4af2-a219-19c8dc7170fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "1017ca21-2c52-4b01-8f1f-34b740909a27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "2af10d29-0ad3-4d7e-a5f4-6117f3d127da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 44
        },
        {
            "id": "dfc843a8-4b65-494a-807d-602db6575cc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "52bfcb56-24df-4da4-bbe1-ad786a7e81e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 46
        },
        {
            "id": "d1bd7dd6-c9fa-4876-82a4-0a8a1d5d5878",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "aceede19-4bec-4291-8420-73ffbd3abe97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "3e0b1a09-05d8-4859-9387-4470c2ff2e13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "7e0d588e-eafb-4707-9c0f-8d0daab9ec58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "b54dfd1b-12a8-4991-83d0-fb95ea29ce7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "8eaadfb6-c017-4f57-8354-5249e96b2422",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "f9e6ac32-c989-4f40-a956-efea69d7a262",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "7b6debd3-1042-4df0-af61-fb47e5cceeb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "18ac6a31-caac-4940-8776-229bf029165a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "36ef25d5-50be-4fb5-a3f7-847ab68caaaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "da8b52c0-6d22-4254-9d8a-243d6696c638",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "81d4d0b2-c777-4f80-82ee-f101f78dbcf8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "f50d9c67-23bb-431b-b764-90a53072498d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "770af0c1-5297-4939-a569-ee2079f93e41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "c95720ae-87d7-4aaf-a47a-f141a80e8e4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "a761b18a-3d0e-4b31-bf96-c87179bc8cb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 8217
        },
        {
            "id": "9e045572-f51c-48ac-ba78-39882dff57fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "58b55e16-6795-4d25-a45d-1200c28e325d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "9b05af23-de26-4c83-be45-a019487d18d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "66fc6674-be26-4d46-822d-e243ecaa0594",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "6136af2f-df33-4ed3-94ef-efc5a092fda5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "f7e0c57c-b6ca-47a3-b497-c1daeb4f4040",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 14,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}