{
    "id": "b39043cd-d4f4-4b19-af5d-ea37db12d2dc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_asteroid_large",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 2,
    "bbox_right": 60,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "70b38d03-7d15-481d-837c-b7f7bc312916",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b39043cd-d4f4-4b19-af5d-ea37db12d2dc",
            "compositeImage": {
                "id": "eab79374-c727-44e6-bcd9-ce9cb44866f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70b38d03-7d15-481d-837c-b7f7bc312916",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b8224a5-7c62-4be2-a7a4-639ccaf12bde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70b38d03-7d15-481d-837c-b7f7bc312916",
                    "LayerId": "841f36dc-a1ce-45bd-8fbd-46da8f5926dc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "841f36dc-a1ce-45bd-8fbd-46da8f5926dc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b39043cd-d4f4-4b19-af5d-ea37db12d2dc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}