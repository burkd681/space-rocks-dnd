{
    "id": "38c3bfe5-81d5-470e-98e4-1be96957e086",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite6",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bae444d0-631c-42b4-8e77-23fccc4b456e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38c3bfe5-81d5-470e-98e4-1be96957e086",
            "compositeImage": {
                "id": "4a537717-b4c1-459b-9b71-b62b172518b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bae444d0-631c-42b4-8e77-23fccc4b456e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7806a94-3510-4697-a5cb-99f9fe5d9884",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bae444d0-631c-42b4-8e77-23fccc4b456e",
                    "LayerId": "0b511e62-edde-4c2b-a7e4-1cfc40e91feb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "0b511e62-edde-4c2b-a7e4-1cfc40e91feb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "38c3bfe5-81d5-470e-98e4-1be96957e086",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}