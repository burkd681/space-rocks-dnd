{
    "id": "d8a65c10-ddc2-4676-bad6-05ab68872945",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 4,
    "bbox_left": 0,
    "bbox_right": 4,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "02f61bbe-8f46-47fd-9186-acce6fdd0cb2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a65c10-ddc2-4676-bad6-05ab68872945",
            "compositeImage": {
                "id": "d453d9a7-402a-4ff2-9800-05e6980d7345",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02f61bbe-8f46-47fd-9186-acce6fdd0cb2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ebf2e8c-b951-4d68-bc0a-b33f64748006",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02f61bbe-8f46-47fd-9186-acce6fdd0cb2",
                    "LayerId": "82b2ae8d-378d-4455-93ed-741c02600dbd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 5,
    "layers": [
        {
            "id": "82b2ae8d-378d-4455-93ed-741c02600dbd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d8a65c10-ddc2-4676-bad6-05ab68872945",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 5,
    "xorig": 0,
    "yorig": 0
}