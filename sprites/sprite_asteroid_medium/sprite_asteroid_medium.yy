{
    "id": "d7d562d6-82bf-459c-927c-f3835988827b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_asteroid_medium",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 1,
    "bbox_right": 30,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fee75079-ad2a-4338-96e4-60838977ace1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7d562d6-82bf-459c-927c-f3835988827b",
            "compositeImage": {
                "id": "b740d0b3-ef57-433e-9015-1fafe6bad9ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fee75079-ad2a-4338-96e4-60838977ace1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09f615f1-151b-41e9-82f7-c60b2fd31d78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fee75079-ad2a-4338-96e4-60838977ace1",
                    "LayerId": "b4cd69d0-29e9-483c-88c6-39e0bf7928f5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b4cd69d0-29e9-483c-88c6-39e0bf7928f5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d7d562d6-82bf-459c-927c-f3835988827b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}