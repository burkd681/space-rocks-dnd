{
    "id": "622768bd-0376-450e-aa6f-3a9fff59d678",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": -2,
    "bbox_right": 63,
    "bbox_top": 14,
    "bboxmode": 2,
    "colkind": 3,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "82a74fbc-c7a0-4179-a251-2da538ce0d89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "622768bd-0376-450e-aa6f-3a9fff59d678",
            "compositeImage": {
                "id": "24f05c5d-0990-404c-a0bb-91b76e9a9844",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82a74fbc-c7a0-4179-a251-2da538ce0d89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd6ab42e-32dc-4a9a-864d-cd2c4509e860",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82a74fbc-c7a0-4179-a251-2da538ce0d89",
                    "LayerId": "afac742e-d463-4c0a-b0c1-cadedbc05ab3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "afac742e-d463-4c0a-b0c1-cadedbc05ab3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "622768bd-0376-450e-aa6f-3a9fff59d678",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}