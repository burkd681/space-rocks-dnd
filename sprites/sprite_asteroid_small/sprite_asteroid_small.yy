{
    "id": "711eef72-b178-4c9e-b92c-78400998e282",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_asteroid_small",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "54719ce9-45dd-4f0a-b8e8-38adba632eb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "711eef72-b178-4c9e-b92c-78400998e282",
            "compositeImage": {
                "id": "f3148ec4-de54-4dab-a939-24d41e58e8ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54719ce9-45dd-4f0a-b8e8-38adba632eb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "088da6dc-9451-49c4-9710-2e5d7a88409c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54719ce9-45dd-4f0a-b8e8-38adba632eb0",
                    "LayerId": "2bf41208-c8d9-4191-a395-b042c4c2aa53"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "2bf41208-c8d9-4191-a395-b042c4c2aa53",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "711eef72-b178-4c9e-b92c-78400998e282",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}