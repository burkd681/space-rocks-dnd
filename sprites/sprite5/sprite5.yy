{
    "id": "b08a3609-8612-4e90-9a83-1093ffb0d086",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "634f5bd7-161b-47da-afb6-2c3009258749",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b08a3609-8612-4e90-9a83-1093ffb0d086",
            "compositeImage": {
                "id": "cb55a22a-4338-49e3-ad78-192b6459ba96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "634f5bd7-161b-47da-afb6-2c3009258749",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c9697c9-528f-4732-9298-5d7d6381da15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "634f5bd7-161b-47da-afb6-2c3009258749",
                    "LayerId": "d6be4881-6c93-400f-a600-48758470201d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "d6be4881-6c93-400f-a600-48758470201d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b08a3609-8612-4e90-9a83-1093ffb0d086",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}