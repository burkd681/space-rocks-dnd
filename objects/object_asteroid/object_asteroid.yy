{
    "id": "2fc701fe-3927-4188-8f42-15c13dc654f2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_asteroid",
    "eventList": [
        {
            "id": "9074f135-4092-4469-ad9e-e2ceecf00c99",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2fc701fe-3927-4188-8f42-15c13dc654f2"
        },
        {
            "id": "537b53b3-5650-45aa-9698-455654f43498",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2fc701fe-3927-4188-8f42-15c13dc654f2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "711eef72-b178-4c9e-b92c-78400998e282",
    "visible": true
}