/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 65CE9620
/// @DnDArgument : "var" "room"
/// @DnDArgument : "value" "rm_game"
if(room == rm_game)
{
	/// @DnDAction : YoYo Games.Instance Variables.If_Score
	/// @DnDVersion : 1
	/// @DnDHash : 1A4E8339
	/// @DnDParent : 65CE9620
	/// @DnDArgument : "op" "4"
	/// @DnDArgument : "value" "1000"
	if(!variable_instance_exists(id, "__dnd_score")) __dnd_score = 0;
	if(__dnd_score >= 1000)
	{
		/// @DnDAction : YoYo Games.Rooms.Go_To_Room
		/// @DnDVersion : 1
		/// @DnDHash : 35277E92
		/// @DnDParent : 1A4E8339
		/// @DnDArgument : "room" "rm_win"
		/// @DnDSaveInfo : "room" "d7232391-2419-4241-b7b5-31511c21be5b"
		room_goto(rm_win);
	}

	/// @DnDAction : YoYo Games.Instance Variables.If_Lives
	/// @DnDVersion : 1
	/// @DnDHash : 6836A7ED
	/// @DnDParent : 65CE9620
	/// @DnDArgument : "op" "3"
	if(!variable_instance_exists(id, "__dnd_lives")) __dnd_lives = 0;
	if(__dnd_lives <= 0)
	{
		/// @DnDAction : YoYo Games.Rooms.Go_To_Room
		/// @DnDVersion : 1
		/// @DnDHash : 3C8AFA7E
		/// @DnDParent : 6836A7ED
		/// @DnDArgument : "room" "rm_gameover"
		/// @DnDSaveInfo : "room" "4efdb8a9-7157-4cb0-90a0-41a15804b7c4"
		room_goto(rm_gameover);
	}
}