/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 31975FAC
/// @DnDArgument : "var" "room"
/// @DnDArgument : "value" "rm_game"
if(room == rm_game)
{
	/// @DnDAction : YoYo Games.Random.Choose
	/// @DnDVersion : 1
	/// @DnDHash : 01753679
	/// @DnDInput : 2
	/// @DnDParent : 31975FAC
	/// @DnDArgument : "var" "choice"
	/// @DnDArgument : "var_temp" "1"
	/// @DnDArgument : "option_1" "1"
	var choice = choose(0, 1);

	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 08F4F11B
	/// @DnDParent : 31975FAC
	/// @DnDArgument : "var" "choice"
	if(choice == 0)
	{
		/// @DnDAction : YoYo Games.Random.Get_Random_Number
		/// @DnDVersion : 1
		/// @DnDHash : 6895A57A
		/// @DnDParent : 08F4F11B
		/// @DnDArgument : "var" "xx"
		/// @DnDArgument : "var_temp" "1"
		/// @DnDArgument : "type" "1"
		/// @DnDArgument : "max" "room_width*0.3"
		var xx = floor(random_range(0, room_width*0.3 + 1));
	}
}