/// @DnDAction : YoYo Games.Switch.Switch
/// @DnDVersion : 1
/// @DnDHash : 3733E710
/// @DnDArgument : "expr" "room"
var l3733E710_0 = room;
switch(l3733E710_0)
{
	/// @DnDAction : YoYo Games.Switch.Case
	/// @DnDVersion : 1
	/// @DnDHash : 1109ACEE
	/// @DnDParent : 3733E710
	/// @DnDArgument : "const" "rm_win"
	case rm_win:
		/// @DnDAction : YoYo Games.Drawing.Set_Alignment
		/// @DnDVersion : 1.1
		/// @DnDHash : 2666AACC
		/// @DnDParent : 1109ACEE
		/// @DnDArgument : "halign" "fa_center"
		draw_set_halign(fa_center);
		draw_set_valign(fa_top);
	
		/// @DnDAction : YoYo Games.Drawing.Set_Color
		/// @DnDVersion : 1
		/// @DnDHash : 777126A3
		/// @DnDParent : 1109ACEE
		/// @DnDArgument : "color" "$FFFF0010"
		draw_set_colour($FFFF0010 & $ffffff);
		var l777126A3_0=($FFFF0010 >> 24);
		draw_set_alpha(l777126A3_0 / $ff);
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Value_Transformed
		/// @DnDVersion : 1
		/// @DnDHash : 45799418
		/// @DnDParent : 1109ACEE
		/// @DnDArgument : "x" "250"
		/// @DnDArgument : "y" "150"
		/// @DnDArgument : "xscale" "3"
		/// @DnDArgument : "yscale" "3"
		/// @DnDArgument : "caption" ""YOU WIN""
		draw_text_transformed(250, 150, string("YOU WIN") + "", 3, 3, 0);
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Value
		/// @DnDVersion : 1
		/// @DnDHash : 2279A691
		/// @DnDParent : 1109ACEE
		/// @DnDArgument : "x" "250"
		/// @DnDArgument : "y" "250"
		/// @DnDArgument : "caption" ""Final Score""
		draw_text(250, 250, string("Final Score") + "");
	
		/// @DnDAction : YoYo Games.Drawing.Set_Color
		/// @DnDVersion : 1
		/// @DnDHash : 1FB9EF2B
		/// @DnDParent : 1109ACEE
		/// @DnDArgument : "color" "$FFFF2E00"
		draw_set_colour($FFFF2E00 & $ffffff);
		var l1FB9EF2B_0=($FFFF2E00 >> 24);
		draw_set_alpha(l1FB9EF2B_0 / $ff);
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Value
		/// @DnDVersion : 1
		/// @DnDHash : 2D4FC29E
		/// @DnDParent : 1109ACEE
		/// @DnDArgument : "x" "250"
		/// @DnDArgument : "y" "300 "
		/// @DnDArgument : "caption" "">>Press ENTER to restart""
		draw_text(250, 300 , string(">>Press ENTER to restart") + "");
	
		/// @DnDAction : YoYo Games.Drawing.Set_Alignment
		/// @DnDVersion : 1.1
		/// @DnDHash : 6EE849C1
		/// @DnDParent : 1109ACEE
		/// @DnDArgument : "halign" "fa_center"
		draw_set_halign(fa_center);
		draw_set_valign(fa_top);
		break;

	/// @DnDAction : YoYo Games.Switch.Case
	/// @DnDVersion : 1
	/// @DnDHash : 45722FCD
	/// @DnDParent : 3733E710
	/// @DnDArgument : "const" "rm_game"
	case rm_game:
		/// @DnDAction : YoYo Games.Drawing.Draw_Instance_Score
		/// @DnDVersion : 1
		/// @DnDHash : 6D8DF547
		/// @DnDParent : 45722FCD
		/// @DnDArgument : "x" "100"
		/// @DnDArgument : "y" "20"
		if(!variable_instance_exists(id, "__dnd_score")) __dnd_score = 0;
		draw_text(100, 20, string("Score: ") + string(__dnd_score));
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Instance_Lives
		/// @DnDVersion : 1
		/// @DnDHash : 729060BD
		/// @DnDParent : 45722FCD
		/// @DnDArgument : "x" "20"
		/// @DnDArgument : "y" "40"
		/// @DnDArgument : "sprite" "sprite6"
		/// @DnDSaveInfo : "sprite" "38c3bfe5-81d5-470e-98e4-1be96957e086"
		var l729060BD_0 = sprite_get_width(sprite6);
		var l729060BD_1 = 0;
		if(!variable_instance_exists(id, "__dnd_lives")) __dnd_lives = 0;
		for(var l729060BD_2 = __dnd_lives; l729060BD_2 > 0; --l729060BD_2) {
			draw_sprite(sprite6, 0, 20 + l729060BD_1, 40);
			l729060BD_1 += l729060BD_0;
		}
		break;

	/// @DnDAction : YoYo Games.Switch.Case
	/// @DnDVersion : 1
	/// @DnDHash : 10D3EA81
	/// @DnDParent : 3733E710
	/// @DnDArgument : "const" "rm_start"
	case rm_start:
		/// @DnDAction : YoYo Games.Drawing.Set_Alignment
		/// @DnDVersion : 1.1
		/// @DnDHash : 55286056
		/// @DnDParent : 10D3EA81
		/// @DnDArgument : "halign" "fa_center"
		draw_set_halign(fa_center);
		draw_set_valign(fa_top);
	
		/// @DnDAction : YoYo Games.Drawing.Set_Color
		/// @DnDVersion : 1
		/// @DnDHash : 5706F84F
		/// @DnDParent : 10D3EA81
		/// @DnDArgument : "color" "$FFFF0010"
		draw_set_colour($FFFF0010 & $ffffff);
		var l5706F84F_0=($FFFF0010 >> 24);
		draw_set_alpha(l5706F84F_0 / $ff);
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Value_Transformed
		/// @DnDVersion : 1
		/// @DnDHash : 47440961
		/// @DnDParent : 10D3EA81
		/// @DnDArgument : "x" "250"
		/// @DnDArgument : "y" "100"
		/// @DnDArgument : "xscale" "3"
		/// @DnDArgument : "yscale" "3"
		/// @DnDArgument : "caption" ""Space Rocks:""
		draw_text_transformed(250, 100, string("Space Rocks:") + "", 3, 3, 0);
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Value
		/// @DnDVersion : 1
		/// @DnDHash : 75053F01
		/// @DnDParent : 10D3EA81
		/// @DnDArgument : "x" "250"
		/// @DnDArgument : "y" "200"
		/// @DnDArgument : "caption" ""Score 1000 Points to Win""
		draw_text(250, 200, string("Score 1000 Points to Win") + "");
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Value
		/// @DnDVersion : 1
		/// @DnDHash : 138C5112
		/// @DnDParent : 10D3EA81
		/// @DnDArgument : "x" "250"
		/// @DnDArgument : "y" "230"
		/// @DnDArgument : "caption" ""Caption: Move wiht up/left/right key""
		draw_text(250, 230, string("Caption: Move wiht up/left/right key") + "");
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Value
		/// @DnDVersion : 1
		/// @DnDHash : 5EFC1066
		/// @DnDParent : 10D3EA81
		/// @DnDArgument : "x" "250"
		/// @DnDArgument : "y" "260"
		/// @DnDArgument : "caption" ""Press SPACE to shoot""
		draw_text(250, 260, string("Press SPACE to shoot") + "");
	
		/// @DnDAction : YoYo Games.Drawing.Set_Color
		/// @DnDVersion : 1
		/// @DnDHash : 2C160567
		/// @DnDParent : 10D3EA81
		draw_set_colour($FFFFFFFF & $ffffff);
		var l2C160567_0=($FFFFFFFF >> 24);
		draw_set_alpha(l2C160567_0 / $ff);
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Value
		/// @DnDVersion : 1
		/// @DnDHash : 4336F03B
		/// @DnDParent : 10D3EA81
		/// @DnDArgument : "x" "200"
		/// @DnDArgument : "y" "400"
		/// @DnDArgument : "caption" ""Press ENTER to START ""
		draw_text(200, 400, string("Press ENTER to START ") + "");
	
		/// @DnDAction : YoYo Games.Drawing.Set_Alignment
		/// @DnDVersion : 1.1
		/// @DnDHash : 1F7A3697
		/// @DnDParent : 10D3EA81
		/// @DnDArgument : "halign" "fa_center"
		draw_set_halign(fa_center);
		draw_set_valign(fa_top);
	
		/// @DnDAction : YoYo Games.Drawing.Set_Color
		/// @DnDVersion : 1
		/// @DnDHash : 74CAA20D
		/// @DnDParent : 10D3EA81
		draw_set_colour($FFFFFFFF & $ffffff);
		var l74CAA20D_0=($FFFFFFFF >> 24);
		draw_set_alpha(l74CAA20D_0 / $ff);
		break;

	/// @DnDAction : YoYo Games.Switch.Case
	/// @DnDVersion : 1
	/// @DnDHash : 2FB1200D
	/// @DnDParent : 3733E710
	/// @DnDArgument : "const" "rm_gameover"
	case rm_gameover:
		/// @DnDAction : YoYo Games.Drawing.Set_Alignment
		/// @DnDVersion : 1.1
		/// @DnDHash : 6B79284B
		/// @DnDParent : 2FB1200D
		/// @DnDArgument : "halign" "fa_center"
		draw_set_halign(fa_center);
		draw_set_valign(fa_top);
	
		/// @DnDAction : YoYo Games.Drawing.Set_Color
		/// @DnDVersion : 1
		/// @DnDHash : 119AC109
		/// @DnDParent : 2FB1200D
		/// @DnDArgument : "color" "$FF0000FF"
		draw_set_colour($FF0000FF & $ffffff);
		var l119AC109_0=($FF0000FF >> 24);
		draw_set_alpha(l119AC109_0 / $ff);
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Value_Transformed
		/// @DnDVersion : 1
		/// @DnDHash : 4EEC029D
		/// @DnDParent : 2FB1200D
		/// @DnDArgument : "x" "250"
		/// @DnDArgument : "y" "150"
		/// @DnDArgument : "xscale" "3"
		/// @DnDArgument : "yscale" "3"
		/// @DnDArgument : "caption" ""YOU LOSE ""
		draw_text_transformed(250, 150, string("YOU LOSE ") + "", 3, 3, 0);
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Value
		/// @DnDVersion : 1
		/// @DnDHash : 13F72312
		/// @DnDParent : 2FB1200D
		/// @DnDArgument : "x" "250"
		/// @DnDArgument : "y" "250"
		/// @DnDArgument : "caption" ""Final Score ""
		draw_text(250, 250, string("Final Score ") + "");
	
		/// @DnDAction : YoYo Games.Drawing.Set_Color
		/// @DnDVersion : 1
		/// @DnDHash : 3CB21D05
		/// @DnDParent : 2FB1200D
		/// @DnDArgument : "color" "$FF0CFF10"
		draw_set_colour($FF0CFF10 & $ffffff);
		var l3CB21D05_0=($FF0CFF10 >> 24);
		draw_set_alpha(l3CB21D05_0 / $ff);
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Value
		/// @DnDVersion : 1
		/// @DnDHash : 1CC4F192
		/// @DnDParent : 2FB1200D
		/// @DnDArgument : "x" "250"
		/// @DnDArgument : "y" "300"
		/// @DnDArgument : "caption" "">>Press ENTER to RESTART ""
		draw_text(250, 300, string(">>Press ENTER to RESTART ") + "");
	
		/// @DnDAction : YoYo Games.Drawing.Set_Alignment
		/// @DnDVersion : 1.1
		/// @DnDHash : 4AC86CED
		/// @DnDParent : 2FB1200D
		/// @DnDArgument : "halign" "fa_center"
		draw_set_halign(fa_center);
		draw_set_valign(fa_top);
		break;
}