/// @DnDAction : YoYo Games.Switch.Switch
/// @DnDVersion : 1
/// @DnDHash : 39F060C0
/// @DnDArgument : "expr" "room"
var l39F060C0_0 = room;
switch(l39F060C0_0)
{
	/// @DnDAction : YoYo Games.Switch.Case
	/// @DnDVersion : 1
	/// @DnDHash : 5D65126A
	/// @DnDParent : 39F060C0
	/// @DnDArgument : "const" "rm_start"
	case rm_start:
		/// @DnDAction : YoYo Games.Rooms.Go_To_Room
		/// @DnDVersion : 1
		/// @DnDHash : 315C67D2
		/// @DnDParent : 5D65126A
		/// @DnDArgument : "room" "rm_game"
		/// @DnDSaveInfo : "room" "2be0acfb-91d1-48e0-9ae6-0ac81b3c27c7"
		room_goto(rm_game);
		break;

	/// @DnDAction : YoYo Games.Switch.Case
	/// @DnDVersion : 1
	/// @DnDHash : 136A481D
	/// @DnDParent : 39F060C0
	/// @DnDArgument : "const" "rm_gameover"
	case rm_gameover:
		/// @DnDAction : YoYo Games.Game.Restart_Game
		/// @DnDVersion : 1
		/// @DnDHash : 0332082D
		/// @DnDParent : 136A481D
		game_restart();
		break;

	/// @DnDAction : YoYo Games.Switch.Case
	/// @DnDVersion : 1
	/// @DnDHash : 0F96E68C
	/// @DnDParent : 39F060C0
	/// @DnDArgument : "const" "rm_win"
	case rm_win:
		/// @DnDAction : YoYo Games.Game.Restart_Game
		/// @DnDVersion : 1
		/// @DnDHash : 57EB36AC
		/// @DnDParent : 0F96E68C
		game_restart();
		break;
}