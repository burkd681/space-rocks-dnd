{
    "id": "429836b6-2fbf-40fd-b621-79c5a85073c6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_game",
    "eventList": [
        {
            "id": "228c5747-c967-451c-a914-575cd42d621f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "429836b6-2fbf-40fd-b621-79c5a85073c6"
        },
        {
            "id": "40a3efa3-10e2-4506-8a3b-5577035e859d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "429836b6-2fbf-40fd-b621-79c5a85073c6"
        },
        {
            "id": "d29a0be4-ee26-4b43-b123-9d74add91511",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "429836b6-2fbf-40fd-b621-79c5a85073c6"
        },
        {
            "id": "c9492722-a74a-4028-8c35-72f72440bc86",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 9,
            "m_owner": "429836b6-2fbf-40fd-b621-79c5a85073c6"
        },
        {
            "id": "af247c70-15f9-4b69-a452-8ca3bfc2eabf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "429836b6-2fbf-40fd-b621-79c5a85073c6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}