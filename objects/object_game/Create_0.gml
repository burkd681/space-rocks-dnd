/// @DnDAction : YoYo Games.Instance Variables.Set_Score
/// @DnDVersion : 1
/// @DnDHash : 093B2EA9
/// @DnDArgument : "score" "900"

__dnd_score = real(900);

/// @DnDAction : YoYo Games.Instance Variables.Set_Lives
/// @DnDVersion : 1
/// @DnDHash : 440F672E
/// @DnDArgument : "lives" "3"

__dnd_lives = real(3);

/// @DnDAction : YoYo Games.Drawing.Set_Font
/// @DnDVersion : 1
/// @DnDHash : 30A53E24
/// @DnDArgument : "font" "fnt_text"
/// @DnDSaveInfo : "font" "9c86d7b2-2f29-4b2a-b22c-8ec519a51ba5"
draw_set_font(fnt_text);