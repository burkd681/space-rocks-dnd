/// @DnDAction : YoYo Games.Common.Apply_To
/// @DnDVersion : 1
/// @DnDHash : 4BE13C02
/// @DnDApplyTo : 429836b6-2fbf-40fd-b621-79c5a85073c6
with(object_game) {
	/// @DnDAction : YoYo Games.Instance Variables.Set_Score
	/// @DnDVersion : 1
	/// @DnDHash : 04339AB4
	/// @DnDParent : 4BE13C02
	/// @DnDArgument : "score" "10"
	/// @DnDArgument : "score_relative" "1"
	if(!variable_instance_exists(id, "__dnd_score")) __dnd_score = 0;
	__dnd_score += real(10);
}

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 59126A8A
instance_destroy();

/// @DnDAction : YoYo Games.Common.Apply_To
/// @DnDVersion : 1
/// @DnDHash : 486EAF90
/// @DnDApplyTo : other
with(other) {
	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 00E9E61D
	/// @DnDParent : 486EAF90
	instance_destroy();

	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 17E42735
	/// @DnDParent : 486EAF90
	/// @DnDArgument : "var" "sprite_index"
	/// @DnDArgument : "value" "sprite_asteroid_large"
	if(sprite_index == sprite_asteroid_large)
	{
		/// @DnDAction : YoYo Games.Loops.Repeat
		/// @DnDVersion : 1
		/// @DnDHash : 0312617C
		/// @DnDParent : 17E42735
		/// @DnDArgument : "times" "2"
		repeat(2)
		{
			/// @DnDAction : YoYo Games.Instances.Create_Instance
			/// @DnDVersion : 1
			/// @DnDHash : 4AC5DDFF
			/// @DnDParent : 0312617C
			/// @DnDArgument : "xpos_relative" "1"
			/// @DnDArgument : "ypos_relative" "1"
			/// @DnDArgument : "var" "newAsteroid"
			/// @DnDArgument : "var_temp" "1"
			/// @DnDArgument : "objectid" "object_asteroid"
			/// @DnDSaveInfo : "objectid" "2fc701fe-3927-4188-8f42-15c13dc654f2"
			var newAsteroid = instance_create_layer(x + 0, y + 0, "Instances", object_asteroid);
		
			/// @DnDAction : YoYo Games.Common.Variable
			/// @DnDVersion : 1
			/// @DnDHash : 650920DF
			/// @DnDParent : 0312617C
			variable = 0;
		}
	}

	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 4ED18844
	/// @DnDParent : 486EAF90
	/// @DnDArgument : "var" "sprite_index"
	/// @DnDArgument : "value" "sprite_asteroid_medium"
	if(sprite_index == sprite_asteroid_medium)
	{
		/// @DnDAction : YoYo Games.Loops.Repeat
		/// @DnDVersion : 1
		/// @DnDHash : 1A0CF08F
		/// @DnDParent : 4ED18844
		/// @DnDArgument : "times" "2"
		repeat(2)
		{
			/// @DnDAction : YoYo Games.Instances.Create_Instance
			/// @DnDVersion : 1
			/// @DnDHash : 28C8DA5A
			/// @DnDParent : 1A0CF08F
			/// @DnDArgument : "xpos_relative" "1"
			/// @DnDArgument : "ypos_relative" "1"
			/// @DnDArgument : "var" "newAsteroid"
			/// @DnDArgument : "var_temp" "1"
			/// @DnDArgument : "objectid" "object_asteroid"
			/// @DnDSaveInfo : "objectid" "2fc701fe-3927-4188-8f42-15c13dc654f2"
			var newAsteroid = instance_create_layer(x + 0, y + 0, "Instances", object_asteroid);
		
			/// @DnDAction : YoYo Games.Common.Variable
			/// @DnDVersion : 1
			/// @DnDHash : 4334F1FE
			/// @DnDParent : 1A0CF08F
			/// @DnDArgument : "expr" "sprite_asteroid_small"
			/// @DnDArgument : "var" "newAsteroid.sprite_index"
			newAsteroid.sprite_index = sprite_asteroid_small;
		}
	}

	/// @DnDAction : YoYo Games.Loops.Repeat
	/// @DnDVersion : 1
	/// @DnDHash : 123ACE19
	/// @DnDParent : 486EAF90
	/// @DnDArgument : "times" "10"
	repeat(10)
	{
		/// @DnDAction : YoYo Games.Instances.Create_Instance
		/// @DnDVersion : 1
		/// @DnDHash : 29955656
		/// @DnDParent : 123ACE19
		/// @DnDArgument : "objectid" "object_debris"
		/// @DnDSaveInfo : "objectid" "9deac153-2022-4005-b163-00ac6454d1c8"
		instance_create_layer(0, 0, "Instances", object_debris);
	}
}