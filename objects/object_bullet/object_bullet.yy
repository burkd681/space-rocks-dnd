{
    "id": "93fb10ff-6289-42c4-99b9-8d774655d1f7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_bullet",
    "eventList": [
        {
            "id": "ec0e58c4-b23b-446d-be03-9aa34ba8e819",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "93fb10ff-6289-42c4-99b9-8d774655d1f7"
        },
        {
            "id": "756e9a43-580e-4201-8ceb-889fbf9858cf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "2fc701fe-3927-4188-8f42-15c13dc654f2",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "93fb10ff-6289-42c4-99b9-8d774655d1f7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d8a65c10-ddc2-4676-bad6-05ab68872945",
    "visible": true
}