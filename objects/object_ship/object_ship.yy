{
    "id": "5ba61781-e44c-4f10-a6df-30170806103e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_ship",
    "eventList": [
        {
            "id": "1a9afe6a-c99e-4c58-976a-f4008bf98ff2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 37,
            "eventtype": 5,
            "m_owner": "5ba61781-e44c-4f10-a6df-30170806103e"
        },
        {
            "id": "e66c3536-2719-4483-a947-7ff23553ecf2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 39,
            "eventtype": 5,
            "m_owner": "5ba61781-e44c-4f10-a6df-30170806103e"
        },
        {
            "id": "d4a0556e-1808-43f2-82fd-66f636049e67",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5ba61781-e44c-4f10-a6df-30170806103e"
        },
        {
            "id": "06a90f19-96cc-4a91-81d4-3b41893244bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 38,
            "eventtype": 5,
            "m_owner": "5ba61781-e44c-4f10-a6df-30170806103e"
        },
        {
            "id": "38741e00-ca9c-445e-9271-6dda97edd35a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 9,
            "m_owner": "5ba61781-e44c-4f10-a6df-30170806103e"
        },
        {
            "id": "0954ad2b-340d-470e-ab5b-2ca4207b43cb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "2fc701fe-3927-4188-8f42-15c13dc654f2",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "5ba61781-e44c-4f10-a6df-30170806103e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "622768bd-0376-450e-aa6f-3a9fff59d678",
    "visible": true
}