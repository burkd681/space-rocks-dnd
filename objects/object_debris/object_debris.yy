{
    "id": "9deac153-2022-4005-b163-00ac6454d1c8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_debris",
    "eventList": [
        {
            "id": "154b0653-daac-4026-ba39-175416c1d64d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9deac153-2022-4005-b163-00ac6454d1c8"
        },
        {
            "id": "d057df6f-c089-49f0-b58b-8bf2fffe9f7b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9deac153-2022-4005-b163-00ac6454d1c8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b08a3609-8612-4e90-9a83-1093ffb0d086",
    "visible": true
}